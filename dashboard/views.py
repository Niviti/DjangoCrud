from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.generic import View
from django.views.generic.base import TemplateView, TemplateResponseMixin, ContextMixin
from django.views.generic.detail import DetailView 
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView, ModelFormMixin


from django.contrib.messages.views import SuccessMessageMixin
from .forms import BookForm
from .models import Book 
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.contrib import messages

## CREATE
## RETREIVE
## UPDATE
## DELETE

## Aby ta metoda działa musimy wrzucić ją na początek kontrolera
## dzieki temu mamy security i nie ktore URL sa zablokowane
class LoginRequiredMixin(object):
      @classmethod
      def as_view(cls, **kwargs):
          view = super(LoginRequiredMixin, cls).as_view(**kwargs)
          return login_required(view)

## Delete Jedyne co musimy zrobić do zaimplementować klase DeleteView
## Oraz wcisnąć nasz model
## Domyślnie program szuka book_delate
class BookDeleteView(DeleteView):
      model = Book
      def get_success_url(self):
               return reverse('books') 

## Update Albo z Fieldsów budujemy formularz
## albo importujemy BookForm
class BookUpdateView(UpdateView):
    model = Book
    #fields = ["title" "description"]
    form_class = BookForm
    template_name = "form.html"

## Defaultowo mozemy sie odwołać do modelu przez object in view
class BookCreate(SuccessMessageMixin, CreateView):
    ## Definiujemy nasz formularz oraz templatke
    template_name = "form.html"
    form_class = BookForm
    ## wrzucamy title oraz created są to pola z modelu działa to tylko dzięki SuccessMassageMixin
    success_message = "udało ci sie stworzyć nową książke"
    
    ## dziala takze z fields
    #fields = ["title", "description"]
    
    ## jezeli formularz jest valid to cisniemy dalej
    def form_valid(self, form):
         ## Dodawanie pola do formularza w kontrolerze 
         form.instance.added_by = self.request.user
         ## nasz update
         valid_form = super(BookCreate, self).form_valid(form)
         
         ## mozemy wyświetlić pole sukcesu tutaj albo w odzielnej metodzie
         #messages.success(self.request, "Książka została stworzona!")
         
         return valid_form
    
    ## jeżeli wszystko jest cacy to przechodzimy do view o nazwie books
    def get_success_url(self):
         return reverse("books")

    #def get_success_message(self, cleaned_data):
    #       return self.success_message % dict(
    #             cleaned_data,
    #             calculated_field = self.object.timestamp,
    #    )      


## Defaultowo mozemy sie odwołać do modelu przez object in view
## Pole Detail z połączeniem formualrza 
class BookDetail(SuccessMessageMixin, ModelFormMixin, DetailView):
    model = Book
    template_name = "book_detail2.html"
    success_message = "%(title)s has been updated"
    form_class = BookForm

    ##zwracamy context        
    def get_context_data(self, *args, **kwargs):
          context= super(BookDetail, self).get_context_data(*args, **kwargs)
          ## self dzieki temu możemy odwołać się do pól  wyżej model, template.. itp
          context["form"] = self.get_form()
          return context
    
    ## jeśli w tym widoku jest metoda post to dzieje sie to wszystko
    def post(self, request, *args, **kwargs):
            if request.user.is_authenticated():
                 self.object = self.get_object()
                 form = self.get_form()
                 if form.is_valid():
                       return self.form_valid(form)
                 else:
                       return self.form_invalid(form)     
                            

## easy wystarczy template oraz model
class BookListView(ListView):
    model = Book
    template_name = "book_list2.html"

## zwykły widok
class DashboardTemplateView(TemplateView):
        template_name = "about.html"

        def get_context_data(self, *args, **kwargs):
            context = super(DashboardTemplateView, self).get_context_data(*args, **kwargs)    
            context["title"] = "This is about us"
            return context

## TemplateResponseMixin bez tego nie działa na template_view w urls
## Metody ContextMixin TemplateResponseMixin dziedziczone po base  
class MyView(LoginRequiredMixin, ContextMixin,TemplateResponseMixin, View):
      
      def get(self, request, *args, **kwargs):
            context = self.get_context_data(**kwargs)
            context["title"] = "Some context data"
            return self.render_to_response(context)


