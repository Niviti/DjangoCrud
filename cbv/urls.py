from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from django.views.generic.base import TemplateView
from dashboard.views import DashboardTemplateView, MyView,\
                     BookDetail, BookListView, BookCreate, BookUpdateView, BookDeleteView

urlpatterns = [
    # Examples:
    url(r'^$', 'newsletter.views.home', name='home'),
    url(r'^contact/$', 'newsletter.views.contact', name='contact'),

    ## slah jest na koncu jest potrzebny przed $ bo zobacz ze create spełnia warunki
    ## Kolejność jest ważna !! przed slugiem powinno miec to co ma znaczenie innaczej create bedzie
    ## traktowane jako slug
    url(r'^book/create/$', BookCreate.as_view(), name='books_create'),
    url(r'^book/(?P<slug>[-\w]+)/update/$', BookUpdateView.as_view(), name='book_update'),
    url(r'^book/(?P<slug>[-\w]+)/delete/$', BookDeleteView.as_view(), name='book_delete'),
    url(r'^book/(?P<slug>[-\w]+)/$', BookDetail.as_view(), name='book_detail'),
    url(r'^book', BookListView.as_view(), name='books'),
    url(r'^someview/$', MyView.as_view(template_name="about.html"), name='cos'),
    url(r'^about/$', DashboardTemplateView.as_view(), name='about'),
    url(r'^team/$', TemplateView.as_view(template_name='team.html'), name='team'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)